package telegramwrapper

import (
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"io"
	"mime/multipart"
	"net/http"
	"strings"
)

var serverUrl string
var serverToken string

type TelegramResp struct {
	Success bool   `json:"success"`
	Context string `json:"context"`
}

var AuthErrorMessage = "client_session is invalid or expired"

func Init(pyServerUrl string, pyServerToken string) {
	serverUrl = pyServerUrl
	serverToken = pyServerToken
}

func SendTelegramRequest(method string, url string, params map[string]string, file io.Reader) (*http.Response, error) {

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	for key, val := range params {
		_ = writer.WriteField(key, val)
	}

	if file != nil {
		part, err := writer.CreateFormFile("media_file", "media_file")
		if err != nil {
			return nil, err
		}
		io.Copy(part, file)
	}

	err := writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, serverUrl+url, body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", serverToken)
	req.Header.Add("Content-Type", writer.FormDataContentType())
	var client http.Client
	return client.Do(req)
}

func CoachAuthenticateInit(coachNumber string) (string, error) {
	resp, err := SendTelegramRequest("GET", "user-session?phone="+coachNumber, nil, nil)
	if err != nil {
		return "", err
	}

	var result struct {
		TelegramResp
		PhoneHashCode string `json:"phone_code_hash"`
	}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return "", err
	}

	if !result.Success {
		return "", errors.New(result.Context)
	}

	return result.PhoneHashCode, nil
}

func CoachAuthenticateConfirm(coachNumber string, phoneCodeHash string, otp string) (string, error) {

	var result struct {
		TelegramResp
		Token string `json:"user_session"`
	}

	resp, err := SendTelegramRequest("POST", "user-session", map[string]string{
		"phone":           coachNumber,
		"code":            otp,
		"phone_code_hash": phoneCodeHash,
	}, nil)

	if err != nil {
		return "", err
	}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return "", err
	}

	if !result.Success {
		return "", errors.New(result.Context)
	}

	return result.Token, nil
}

func SendMessageToNums(authToken string, numbers []string, message string) (bool, error) {
	resp, err := SendTelegramRequest("POST", "send-message", map[string]string{
		"client_session": authToken,
		"entities":       strings.Join(numbers, ","),
		"message":        message,
	}, nil)

	if err != nil {
		return false, err
	}

	var result struct {
		TelegramResp
		Message string `json:"message"`
	}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return false, err
	}

	if !result.Success {
		return result.Message == AuthErrorMessage, errors.New(result.Context)
	}

	return false, nil
}

func SendMediaToNums(authToken string, numbers []string, message string, file io.Reader) (bool, error) {
	resp, err := SendTelegramRequest("POST", "send-message", map[string]string{
		"client_session": authToken,
		"entities":       strings.Join(numbers, ","),
		"message":        message,
	}, file)

	if err != nil {
		return false, err
	}

	var result struct {
		TelegramResp
		Message string `json:"message"`
	}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return false, err
	}

	if !result.Success {
		return result.Message == AuthErrorMessage, errors.New(result.Context)
	}

	return false, nil
}

func CreateTelegramGroup(authToken string, numbers []string, groupName string) (int, error) {
	resp, err := SendTelegramRequest("POST", "create-group", map[string]string{
		"client_session": authToken,
		"entities":       strings.Join(numbers, ","),
		"group_title":    groupName,
	}, nil)

	if err != nil {
		return 0, err
	}

	var result struct {
		TelegramResp
		GroupId int `json:"group_id"`
	}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return 0, err
	}

	if !result.Success {
		return 0, errors.New(result.Context)
	}

	return result.GroupId, nil
}

func AddToTelegramGroup(authToken string, numbers []string, groupId int) error {
	resp, err := SendTelegramRequest("POST", "add-group-users", map[string]string{
		"client_session": authToken,
		"entities":       strings.Join(numbers, ","),
		"group_id":    string(groupId),
	}, nil)

	if err != nil {
		return err
	}

	var result TelegramResp
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return err
	}

	if !result.Success {
		return errors.New(result.Context)
	}

	return nil
}